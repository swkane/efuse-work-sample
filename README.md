# Welcome to eFuse Social

This project is a work sample for Sam Kane.
Hosted at: https://swkane.gitlab.io/efuse-work-sample/

## Overview

Welcome to my eFuse Social App! I enjoyed getting to build this small application from the mocks you sent me. I made some architectural decisions along the way that I wanted to provide additional context for here. Some of the major choices I made for this work sample include the following:

* Component Composition / State Management
* Testing Library
* Styling / Component Library
* Deployment

1. **Component Composition**
    1. I chose to use functional components with hooks and Local State to build this sample. I considered using Context or Redux, however, for how few components were necessary for this app, I thought Local State would make for the cleanest solution. If the component tree grew or async calls were added to the project, I believe this would make a case for introducing one of these state management tools.
        1. The one place I did decide to use a little bit of Context was to provide the application with a theme for the screen width breakpoints used to determine mobile responsiveness.
    1. I also chose to leverage the same component with some conditional logic for both parent posts and secondary comments since their designs are very similar and have a lot of shared code.

2. **Testing Library**
    1. I used the React Testing Library to test the functionality of my application because it is widely used, it is already included in the Create React App bundle, and it provides intuitive tools to manipulate and test your components.

3. **Styling / Component Library**
    1. There are many ways to style your React application and they have gone in and out of style over the years. In past projects, I have often used Material UI to handle our theme, styling, and base components. However, I know that the eFuse team doesn’t use Material UI or a component library to style your components so I primarily used Material UI to structure how I styled my components while doing most of the styling by hand.

4. **Deployment**
    1. I used Gitlab Pages to deploy my application because they provide easy to set up, free hosting with incorporated CI/CD as long as you provide a simple `.gitlab-ci.yml` file. As mentioned above, you can view this project at: https://swkane.gitlab.io/efuse-work-sample/

*Thank you for taking the time to review my work sample and I would be happy to answer any questions you might have!*

