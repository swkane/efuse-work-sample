import { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { v4 as uuidv4 } from 'uuid';
import { Avatar, Box } from '@mui/material';

import EllipsisIcon from '../icons/ellip.png';
import HypeIcon from '../icons/hype.png';
import LikedHypeIcon from '../icons/hyped.png';
import PlusIcon from '../icons/plus.png';
import ShareIcon from '../icons/share.png';
import CommentIcon from '../icons/comment.png';
import ProfileImage from '../images/profile.png';

const useStyles = makeStyles(() => ({
    container: {
        marginTop: "5px"
    },
    topRow: {
        display: "flex",
        justifyContent: "space-between",
    },
    labelContainer: {
        display: "flex"
    },
    postLabel: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        marginLeft: "16px"
    },
    postProfilePhoto: {
        width: "60px",
        height: "60px"
    },
    commentProfilePhoto: {
        width: "60px",
        height: "60px"
    },
    title: {
        fontSize: "18px",
        lineHeight: "24px",
        fontWeight: "500",
        color: "#12151D"
    },
    postBody: {
        marginTop: "16px"
    },
    bottomRow: {
        display: "flex",
        padding: "16px 0",
        alignItems: "center",
        fontFamily: "Poppins, sans-serif",
    },
    secondaryButton: {
        fontFamily: "Poppins, sans-serif",
    },
    likeButton: {
        width: "12px",
        height: "16px",
        cursor: "pointer",
    },
    icon: {
        marginRight: "11px"
    },
    commentButton: {
        width: "16px",
        height: "16px"
    },
    shareButton: {
        width: "14px",
        height: "16px"
    },
    plusButton: {
        width: "18px",
        height: "18px"
    },
    ellipButton: {
        width: "20px",
    },
    buttonLabel: {
        marginRight: "11px",
        marginLeft: "5px",
        fontWeight: "200",
        color: "gray",
        fontSize: "10px"
    },
    hypedLabel: {
        color: "#F44900"
    },
    commentInput: {
        padding: "0 5px 0 16px",
        display: 'flex',
        alignItems: 'center',
        justifyContent: "space-between",
        backgroundColor: "#F1F4F8",
        borderRadius: "100px",
        height: "40px",
    },
    selectedCommentInput: {
        border: "1px solid #006CFA",
        backgroundColor: "inherit"
    },
    commentTextInputContainer: {
        display: 'flex',
        alignItems: 'center',
        width: "100%",
    },
    commentTextInput: {
        border: 'none',
        width: "inherit",
        backgroundColor: "inherit",
        fontFamily: "Poppins, san-serif",
        fontSize: "14px",
        "&:focus": {
            outline: "none"
        },
    },
    postButton: {
        border: "none",
        color: "inherit",
        marginRight: "11px",
        backgroundColor: "inherit"
    },
    postEnabled: {
        color: "#006CFA"
    }
}))

const PostOrComment = ({ postInfo, componentType, setSocialFeed, postId }) => {
    const [commentText, setCommentText] = useState("");
    const [isCommentFocused, setIsCommentFocused] = useState(false);
    const [isLiked, setIsLiked] = useState(false);
    const classes = useStyles();

    const handleChange = event => {
        setCommentText(event.target.value);
    }

    const handleCommentFocus = () => {
        setIsCommentFocused(true);
    }

    const handleCreateComment = event => {
        event.preventDefault();

        if (commentText.length === 0) return;

        const id = uuidv4();
        const comment = {
            id,
            body: commentText,
            username: "Nick Mercs",
            postTime: "2 minutes ago",
            hypes: 0,
            shares: 0,
            replies: []
        };

        setSocialFeed(prevState => {
            return { ...prevState, [postInfo.id]: { ...prevState[postInfo.id], comments: { ...prevState[postInfo.id].comments, [id]: comment } } }
        });

        setCommentText("");
    }

    const handleLike = () => {
        setIsLiked(prevState => !prevState);

        setSocialFeed(prevState => {
            let changeInHype = -1;
            if (isLiked === false) { changeInHype = 1 };
            if (componentType === "post") {
                return { ...prevState, [postInfo.id]: { ...prevState[postInfo.id], hypes: prevState[postInfo.id].hypes + changeInHype } }
            } else {
                const currentPost = prevState[postId];
                const currentComment = prevState[postId].comments[postInfo.id];

                return { ...prevState, [postId]: { ...currentPost, comments: { ...currentPost.comments, [currentComment.id]: { ...currentComment, hypes: currentComment.hypes + changeInHype } } } }
            }
        })
    }

    return (
        <Box className={classes.container}>
            <div className={classes.topRow}>
                <div className={classes.labelContainer}>
                    <Avatar sx={componentType === "post" ? { width: 60, height: 60 } : { width: 40, height: 40 }} alt="profile photo" src={ProfileImage} />
                    <div className={classes.postLabel}>
                        <div className={classes.title}>{postInfo.username}</div>
                        <div>{postInfo.postTime}</div>
                    </div>
                </div>
                {componentType === "post" && (
                    <div>
                        <img className={`${classes.icon}  ${classes.ellipButton}`} src={EllipsisIcon} alt="ellipsis icon" />
                    </div>
                )}
            </div>
            <div className={classes.postBody}>
                {postInfo.body}
            </div>
            <div className={classes.bottomRow}>
                <img onClick={handleLike} className={`${classes.icon} ${classes.likeButton}`} src={isLiked ? LikedHypeIcon : HypeIcon} alt="hype icon" /><span className={isLiked ? classes.hypedLabel : undefined}>{postInfo.hypes}</span> <span className={classes.buttonLabel}>Hypes</span>
                <img className={`${classes.icon} ${classes.commentButton}`} src={CommentIcon} alt="comment icon" /><span>{componentType === "post" ? Object.keys(postInfo.comments).length : postInfo.replies.length}</span><span className={classes.buttonLabel}>Comments</span>
                <img className={`${classes.icon} ${classes.shareButton}`} src={ShareIcon} alt="share icon" /><span>{postInfo.shares}</span><span className={classes.buttonLabel}>Shares</span>
                {componentType === "post" && (
                    <>
                        <span>{postInfo.views}</span><span className={classes.buttonLabel}>Views</span>
                    </>
                )}
            </div>
            {componentType === "post" && (
                <form
                    component="form"
                    className={`${classes.commentInput} ${isCommentFocused && classes.selectedCommentInput}`}
                    onClick={handleCommentFocus}
                    onSubmit={handleCreateComment}
                >
                    <div className={classes.commentTextInputContainer}>
                        {!isCommentFocused && <img className={`${classes.icon} ${classes.commentButton}`} src={CommentIcon} alt="comment icon" />}

                        <input placeholder="Add comment"
                            className={classes.commentTextInput}
                            onChange={handleChange}
                            value={commentText}
                        />
                    </div>
                    {isCommentFocused ? <button type="submit" className={`${classes.postButton} ${commentText.length > 0 && classes.postEnabled}`}>Post</button> : <img className={`${classes.icon} ${classes.plusButton}`} src={PlusIcon} alt="add comment" />}
                </form>)
            }
        </Box >
    )
}

export default PostOrComment;