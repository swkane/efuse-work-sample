import { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Box } from '@mui/material';
import { v4 as uuidv4 } from 'uuid';

import CameraIcon from '../icons/camera.png';
import VideoIcon from '../icons/video.png';

const useStyles = makeStyles((theme) => ({
    container: {
        border: "1px solid #CED7E7",
        borderRadius: "10px",
        padding: "16px",
        font: "14px/20px Poppins, sans-serif",
        color: "rgba(18, 21, 29, 0.6)"
    },
    textInput: {
        height: "50px",
        width: "97%",
        border: "1px solid #CED7E7",
        borderRadius: "8px",
        paddingLeft: "20px",
        font: "14px/20px Poppins, sans-serif",
        [theme.breakpoints.down('sm')]: {
            width: "92%"
        }
    },
    bottomRow: {
        display: "flex",
        justifyContent: "space-between",
        paddingTop: "16px",
        alignItems: "center"
    },
    icon: {
        marginRight: "8px",
        width: "14px",
    },
    buttonSecondary: {
        marginRight: "20px"
    },
    buttonPrimary: {
        alignSelf: "flex-end",
        width: "80px",
        background: "#006CFA",
        borderRadius: "8px",
        color: "white",
        fontSize: "12px",
        fontWeight: "600",
        borderStyle: "none",
        height: "39px"
    },
}))

const CreatePost = ({ setSocialFeed }) => {
    const [postBody, setPostBody] = useState("");
    const classes = useStyles();

    const handleChange = event => {
        setPostBody(event.target.value);
    }

    const handlePost = event => {
        event.preventDefault();

        const id = uuidv4();
        const post = {
            id,
            body: postBody,
            username: "Nick Mercs",
            postTime: "2 minutes ago",
            hypes: 0,
            shares: 0,
            views: 0,
            comments: []
        };

        setSocialFeed(prevState => {
            return { ...prevState, [id]: post }
        });

        setPostBody("");
    }

    return (
        <div className={classes.container}>
            <Box>
                <form onSubmit={handlePost}>
                    <input className={classes.textInput} placeholder="What's on your mind?" value={postBody} onChange={handleChange} />
                    <div className={classes.bottomRow}>
                        <div>
                            <span className={classes.buttonSecondary}><img className={classes.icon} alt="camera" src={CameraIcon} />Add Media</span>
                            <span className={classes.buttonSecondary}><img className={classes.icon} alt="video" src={VideoIcon} />Go Live</span>
                        </div>
                        <button type="submit" className={classes.buttonPrimary}>Post</button>
                    </div>
                </form>
            </Box>
        </div>
    )
}

export default CreatePost;