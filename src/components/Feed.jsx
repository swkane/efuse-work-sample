import { makeStyles } from '@mui/styles';
import { Divider } from '@mui/material';

import PostOrComment from './PostOrComment';

const useStyles = makeStyles(() => ({
    container: {
        border: "1px solid #CED7E7",
        borderRadius: "10px",
        padding: "20px 20px 30px 30px",
        fontSize: "12px",
        lineHeight: "18px",
        fontWeight: "400",
        marginTop: "16px",
        color: "rgba(18, 21, 29, 0.87)"
    },
    divider: {
        margin: "16px 0"
    }
}))

const Feed = ({ socialFeed, setSocialFeed }) => {
    const classes = useStyles();

    return (
        <>
            {Object.keys(socialFeed).reverse().map(postId => {
                let commentsObject = socialFeed[postId].comments;

                return (
                    <div key={postId} className={classes.container}>
                        <PostOrComment postInfo={socialFeed[postId]} setSocialFeed={setSocialFeed} componentType={"post"} />
                        {Object.keys(commentsObject).length > 0 && (<div className={classes.divider}>
                            <Divider />
                        </div>)}
                        {Object.keys(commentsObject).map(commentId => (
                            <PostOrComment key={commentId} postInfo={commentsObject[commentId]} setSocialFeed={setSocialFeed} componentType={"comment"} postId={postId} />
                        ))}
                    </div>
                )
            })}
        </>
    )
}

export default Feed;