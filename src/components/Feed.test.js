import { render, screen } from '@testing-library/react'
import Feed from './Feed'

let mockSocialFeed = {
    "1": {
        id: "1",
        body: "this is a test post",
        username: "Nick Mercs",
        postTime: "2 minutes ago",
        hypes: 10,
        shares: 5,
        views: 100,
        comments: {
            "1c": {
                id: "1c",
                body: "this is a test comment",
                username: "Title goes here",
                postTime: "Secondary title goes here",
                hypes: 100,
                shares: 12,
                replies: []
            }
        }
    },
}

test('Test Display Posts - Feed should display posts of data passed to it', () => {
    render(<Feed socialFeed={mockSocialFeed} />);

    expect(screen.getByText(mockSocialFeed["1"].body)).toBeDefined();
})