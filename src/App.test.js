import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import App from './App'

import { createTheme, ThemeProvider } from '@mui/material/styles';
import { themeObj } from './theme';

const theme = createTheme(themeObj);


test('Test Create Post - Feed should display created posts', () => {
  render(<ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>);

  expect(screen.queryByText("this is a test post")).toBeNull();

  let testPostBody = "this is a test post";

  userEvent.type(screen.getByPlaceholderText("What's on your mind?"), testPostBody);
  userEvent.click(screen.getByRole('button', { name: "Post" }));


  expect(screen.getByText(testPostBody)).toBeDefined();
})