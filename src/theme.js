export const themeObj = {
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 880,
            lg: 1200,
            xl: 1536,
        },
    },
}