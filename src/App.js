import { useState } from 'react';
import CreatePost from './components/CreatePost';
import Feed from "./components/Feed";
import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
  app: {
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: "50px 0"
  },
  container: {
    width: "90vw",
    [theme.breakpoints.up('md')]: {
      width: "800px",
    },
  }
}))

function App() {
  const classes = useStyles();

  const [socialFeed, setSocialFeed] = useState({
    "d9a0a474-858f-479e-ac03-42591b5a5c92": {
      id: "d9a0a474-858f-479e-ac03-42591b5a5c92",
      body: "Donec ullamcorper nulla non metus auctor fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis.",
      username: "Nick Mercs",
      postTime: "2 minutes ago",
      hypes: 10,
      shares: 5,
      views: 100,
      comments: {
        "7d179142-08e0-434b-9b5d-ff15d74818d6": {
          id: "7d179142-08e0-434b-9b5d-ff15d74818d6",
          body: "Donec ullamcorper nulla non metus auctor fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis.",
          username: "Title goes here",
          postTime: "Secondary title goes here",
          hypes: 100,
          shares: 12,
          replies: []
        }
      }
    },
  })

  return (
    <div className={classes.app}>
      <div className={classes.container}>
        <CreatePost setSocialFeed={setSocialFeed} />
        <Feed socialFeed={socialFeed} setSocialFeed={setSocialFeed} />
      </div>
    </div>
  );
}

export default App;
